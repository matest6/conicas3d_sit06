﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas.FuncionAleatoria
{
    [CreateAssetMenu(menuName = "Funciones Conicas/ Funcion aleatoria Circunferencia")]
    public class FuncionAleatoriaCircunferencia : ScriptableObject
	{

        #region members

        [SerializeField]
        private float h;

        [SerializeField]
        private float k;

        [SerializeField]
        private float r;

        [SerializeField]
        private FormaCircunferencia formaCircunferencia;

        #endregion

        #region accesors

        public float _h
        {
            get
            {
                return h;
            }
        }

        public float _k
        {
            get
            {
                return k;
            }
        }

        public float _r
        {
            get
            {
                return r;
            }
        }


        public FormaCircunferencia _formaCircunferencia
        {
            get
            {
                return formaCircunferencia;
            }
        }

        #endregion

    }

    public enum FormaCircunferencia
    {
        circular,
        ovalada,
        plana
    }
}