﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas.FuncionAleatoria
{
    [CreateAssetMenu(menuName = "Funciones Conicas/ Funcion aleatoria Parabola")]
    public class FuncionAleatoriaParabola : ScriptableObject
	{
        #region members

        [SerializeField]
        private float h;

        [SerializeField]
        private float k;

        [SerializeField]
        private float p;

        [SerializeField]
        private DireccionParabola direccionParabola;

        #endregion

        #region accesors
        public float _h
        {
            get
            {
                return h;
            }
        }

        public float _k
        {
            get
            {
                return k;
            }
        }

        public float _p
        {
            get
            {
                return p;
            }
        }

        public DireccionParabola _direccionParabola
        {
            get
            {
                return direccionParabola;
            }
        }
        #endregion

    }

    public enum DireccionParabola
    {
        noDefinido,
        arriba, 
        abajo,
        izquierda,
        derecha
    }
}