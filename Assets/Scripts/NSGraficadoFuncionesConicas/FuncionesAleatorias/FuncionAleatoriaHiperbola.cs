﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas.FuncionAleatoria
{
    [CreateAssetMenu(menuName = "Funciones Conicas/ Funcion aleatoria Hiperbola")]
	public class FuncionAleatoriaHiperbola : ScriptableObject
	{

        #region members

        [SerializeField]
        private float h;

        [SerializeField]
        private float k;

        [SerializeField]
        private float a;

        [SerializeField]
        private float b;

        [SerializeField]
        private SentidoHiperbola sentidoHiperbola;

        #endregion

        #region accesors

        public float _h
        {
            get
            {
                return h;
            }
        }

        public float _k
        {
            get
            {
                return k;
            }
        }

        public float _a
        {
            get
            {
                return a;
            }
        }

        public float _b
        {
            get
            {
                return b;
            }
        }

        public SentidoHiperbola _sentidoHiperbola
        {
            get
            {
                return sentidoHiperbola;
            }
        }

        #endregion
    }

    public enum SentidoHiperbola
    {
        noDefinido,
        vertical,
        horizontal
    }
}