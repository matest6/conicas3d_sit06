﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas.FuncionAleatoria
{    
    [CreateAssetMenu(menuName = "Funciones Conicas/ Funcion aleatoria Recta")]
	public class FuncionAleatoriaRecta : ScriptableObject
	{
        #region members

        [SerializeField]
        private float m;

        [SerializeField]
        private float b;
 		#endregion

 		#region accesors

        public float _m
        {
            get
            {
                return m;
            }
        }

        public float _b
        {
            get
            {
                return b;
            }
        }
        #endregion
    }
}