﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
	public class RotacionCuerpoCeleste : MonoBehaviour
	{

        #region members

        [SerializeField]
        private float velocidadRotacion;
        #endregion

        #region monoBehaviour

        // Update is called once per frame
        void Update ()
		{
            EjecutarRotacionCuerpoCeleste();
        }

 		#endregion

 		#region private methods

        private void EjecutarRotacionCuerpoCeleste()
        {
            transform.Rotate(0, velocidadRotacion*Time.deltaTime, 0, Space.Self);
        }
 		#endregion
	}
}