﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
    public class FuncionRectaSinPendiente : AbstractDibujoFuncion
    {
        #region members

        private float r;

        #endregion

        #region accesors

        public float _r
        {
            set
            {
                r = value;
            }
        }
        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake()
        {

        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        #endregion

        #region private methods

        #endregion

        #region public methods

        public override Vector3[] CalcularCordenadasFuncionMatematica()
        {
            var tmpListaPosicionesGrafica = new List<Vector3>();

            for (float tmpY = -1; tmpY <= 1; tmpY += 0.025f)
            {
                tmpListaPosicionesGrafica.Add(new Vector2(0.1f - r, tmpY));
            }

            posicionesRutaXY = tmpListaPosicionesGrafica.ToArray();
            return posicionesRutaXY;
        }

        public override void DibujarFuncion(Material argMaterialDibujo, bool argDibujarProgresivamente = true)
        {
            CrearLineRenderer("LineaRectaSinPendiente", argMaterialDibujo, transform);
            SetPositionLineRenderer(posicionesRutaXY, argDibujarProgresivamente);
        }
        #endregion

        #region courutines

        #endregion
    }
}