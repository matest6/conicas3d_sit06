﻿using NSEvaluacion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NSTraduccionIdiomas;

namespace NSCreacionPDF
{
    /// <summary>
    /// Clase que contiene todos los textos UI y objetos UI del PDF que necesitan asignacion de valores con respecto a lo que sucedio en el simulador
    /// </summary>
    public class ControladorValoresPDF : MonoBehaviour
    {
        #region members

        [SerializeField, Header("Valores y datos del usuario"), Space(10)]
        private Text textUsuarioValor;

        [SerializeField]
        private Text textInstitucionValor;

        [SerializeField]
        private Text textSituacionValor;

        [SerializeField]
        private Text textCursoValor;

        [SerializeField]
        private Text textUnidadValor;

        [SerializeField]
        private Text textIDCursoValor;

        [SerializeField]
        private Text textFechaInicioValor;

        [SerializeField]
        private Text textTiempoPracticaValor;

        [SerializeField]
        private Text textIntentosValor;

        [SerializeField]
        private Text textCalificacionValor;

        private DiccionarioIdiomas DicIoIdiomas;

        /// <summary>
        /// Transform padre donde deben ser creados los inputs llenados por el usuario en la situacion
        /// </summary>
        [SerializeField, Header("Datos funcion seleccionada"), Space(10)]
        private Transform transformInputsFuncionSeleccionada;
        /// <summary>
        /// Trasnform padre donde debe ser creada la funcion que selecciono el usuario
        /// </summary>
        [SerializeField]
        private Transform imageContenedorFuncionSeleccionada;
        /// <summary>
        /// transform padre donde deben ser creados los registros de datos
        /// </summary>
        [SerializeField, Header("Registro Datos situacion seleccionada"), Space(10)]
        private Transform contenedorRegistroDatos;

        [SerializeField]
        private RawImage imageCapturaPlanoCartesiano;

        [SerializeField, Header("Preguntas evaluacion"), Space(10)]
        private Image imageEnunciadoEvaluacion;

        [SerializeField]
        private Text textEnunciadoEvaluacion;

        /// <summary>
        /// transform padre donde deben ser creadas las preguntas de evaluacion
        /// </summary>
        [SerializeField]
        private Transform transformPreguntasEvaluacion;
        /// <summary>
        /// transform padre donde deben ser creadas las respuestas del usuario 
        /// </summary>
        [SerializeField]
        private Transform transformListaRespuestasUsuario;
        /// <summary>
        /// transform padre en donde deben ser creados los numeros de las respuestas del usuario
        /// </summary>
        [SerializeField]
        private Transform transformEnumeracionRespuestasUsuario;
        /// <summary>
        /// transform padre donde deben ser creadas las respuestas correctas de la evaluacion
        /// </summary>
        [SerializeField]
        private Transform transformListaRespuestasCorrectas;
        /// <summary>
        /// transform padre en donde deben ser creados los numeros de las respuestas correctas
        /// </summary>
        [SerializeField]
        private Transform transformEnumeracionRespuestasCorrectas;

        [SerializeField]
        private GameObject prefabTextPreguntaRespuesta;

        [SerializeField]
        private GameObject prefabTextEnumeracion;

        [SerializeField]
        private GameObject prefabContenedorImageRespuesta;

        [SerializeField, Header("Preguntas cuaderno"), Space(10)]
        private GameObject prefabPreguntaCuaderno;

        [SerializeField]
        private Transform transformPadrePreguntasCuaderno;

        private Text preguntasCuaderno;
        //private Text[] preguntasCuaderno;
        #endregion

        #region MonoBehaviour
        private void Awake()
        {
            DicIoIdiomas = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        }

        #endregion

        #region public methods

        public void SetUsuario(string argUsuario)
        {
            textUsuarioValor.text = argUsuario;
        }

        public void SetInstitucion(string argInstitucion)
        {
            textInstitucionValor.text = argInstitucion;
        }

        public void SetSituacion(string argSituacion)
        {
            textSituacionValor.text = argSituacion;
        }

        public void SetCurso(string argCurso)
        {
            textCursoValor.text = argCurso;
        }

        public void SetUnidad(string argUnidad)
        {
            textUnidadValor.text = argUnidad;
        }

        public void SetIDCurso(string argIDCurso)
        {
            textIDCursoValor.text = argIDCurso;
        }

        public void SetFechaInicio(string argFechaInicio)
        {
            textFechaInicioValor.text = argFechaInicio;
        }

        public void SetTiempoPractica(string argTiempoPractica)
        {
            textTiempoPracticaValor.text = argTiempoPractica;
        }

        public void SetIntentos(string argIntentos)
        {
            textIntentosValor.text = argIntentos;
        }

        public void SetCalificacion(string argCalificacion)
        {
            textCalificacionValor.text = argCalificacion;
        }

        public void SetSpriteFuncionSeleccionada(GameObject argSpriteFuncionSeleccionada, GameObject argSpriteFuncionSeleccionadaIncorrecta)
        {
            while (imageContenedorFuncionSeleccionada.transform.childCount > 0)            
                DestroyImmediate(imageContenedorFuncionSeleccionada.transform.GetChild(0).gameObject);
                
            Instantiate(argSpriteFuncionSeleccionada, imageContenedorFuncionSeleccionada);
            Instantiate(argSpriteFuncionSeleccionadaIncorrecta, imageContenedorFuncionSeleccionada);
        }

        public void SetInputsFuncionSeleccionada(GameObject argInputsFuncionSeleccionada)
        {
            if (transformInputsFuncionSeleccionada.childCount == 2)
                Destroy(transformInputsFuncionSeleccionada.GetChild(1).gameObject);

            Instantiate(argInputsFuncionSeleccionada, transformInputsFuncionSeleccionada);
        }

        public void SetInputsRegistroDatos(GameObject argInputsRegistroDatos)
        {
            if (contenedorRegistroDatos.childCount == 1)
                Destroy(contenedorRegistroDatos.GetChild(0).gameObject);

            Instantiate(argInputsRegistroDatos, contenedorRegistroDatos);
        }

        public void SetCapturaGraficaFuncion(Texture argCapturaGraficaFuncion)
        {
            imageCapturaPlanoCartesiano.texture = argCapturaGraficaFuncion;
        }

        public void SetPreguntasSituacion(PreguntasSituacion argPreguntasSituacion, string[] argRespuestasUsuario, Sprite[] argRespuestasImagenUsuario)
        {
            //enunciado evaluacion
            imageEnunciadoEvaluacion.sprite = argPreguntasSituacion._imagenEnunciado;
            textEnunciadoEvaluacion.text = DicIoIdiomas.Traducir( argPreguntasSituacion._textoEnunciado,"");
            //Debug.Log(DicIoIdiomas.Traducir(argPreguntasSituacion._textoEnunciado, ""));

            //borrar preguntas antiguas
            while (transformPreguntasEvaluacion.childCount > 1)
                DestroyImmediate(transformPreguntasEvaluacion.GetChild(1).gameObject);

            //asignar preguntas nuevas.
            for (int i = 0; i < argPreguntasSituacion._preguntas.Length; i++)
            {
                var tmpTextPreguntaEvaluacion = Instantiate(prefabTextPreguntaRespuesta, transformPreguntasEvaluacion).GetComponent<Text>();
                tmpTextPreguntaEvaluacion.text = DicIoIdiomas.Traducir(argPreguntasSituacion._preguntas[i],"");
                //Debug.Log( DicIoIdiomas.Traducir(argPreguntasSituacion._preguntas[i], ""));
            }

            //borrar respuestas correctas antiguas
            while (transformListaRespuestasCorrectas.childCount > 0)
                DestroyImmediate(transformListaRespuestasCorrectas.GetChild(0).gameObject);

            while (transformEnumeracionRespuestasCorrectas.childCount > 0)
                DestroyImmediate(transformEnumeracionRespuestasCorrectas.GetChild(0).gameObject);

            //asignar respuestas correctas nuevas.
            for (int i = 0; i < argPreguntasSituacion._respuestas.Length; i++)
            {
                var tmpStringRespuesta = argPreguntasSituacion._respuestas[i];

                if (string.IsNullOrEmpty(tmpStringRespuesta))
                {
                    var tmpTextRespuestaCorrectaEvaluacion = Instantiate(prefabContenedorImageRespuesta, transformListaRespuestasCorrectas).GetComponent<Transform>().Find("ImageRespuesta").GetComponent<Image>();
                    tmpTextRespuestaCorrectaEvaluacion.sprite = argPreguntasSituacion._respuestasImagen[i];
                    tmpTextRespuestaCorrectaEvaluacion.SetNativeSize();
                    tmpTextRespuestaCorrectaEvaluacion.transform.localScale = Vector3.one * 0.5f;
                }
                else
                {
                    var tmpTextRespuestaCorrectaEvaluacion = Instantiate(prefabTextPreguntaRespuesta, transformListaRespuestasCorrectas).GetComponent<Text>();
                    tmpTextRespuestaCorrectaEvaluacion.text = DicIoIdiomas.Traducir(argPreguntasSituacion._respuestas[i], argPreguntasSituacion._respuestas[i]);
                }                

                var tmpTextEnumeracion = Instantiate(prefabTextEnumeracion, transformEnumeracionRespuestasCorrectas).GetComponent<Text>();
                tmpTextEnumeracion.text = (i + 1) + ". ";
            }

            //borrar respuestas usuario antiguas
            while (transformListaRespuestasUsuario.childCount > 0)
                DestroyImmediate(transformListaRespuestasUsuario.GetChild(0).gameObject);

            while (transformEnumeracionRespuestasUsuario.childCount > 0)
                DestroyImmediate(transformEnumeracionRespuestasUsuario.GetChild(0).gameObject);

            //asignar respuestas usuario nuevas.
            for (int i = 0; i < argPreguntasSituacion._contenedorRespuestasFalsas.Length; i++)
            {
                var tmpStringRespuesta = argPreguntasSituacion._respuestas[i];

                if (string.IsNullOrEmpty(tmpStringRespuesta))
                {
                    var tmpTextRespuestaCorrectaEvaluacion = Instantiate(prefabContenedorImageRespuesta, transformListaRespuestasUsuario).GetComponent<Transform>().Find("ImageRespuesta").GetComponent<Image>();
                    tmpTextRespuestaCorrectaEvaluacion.sprite = argRespuestasImagenUsuario[i];
                    tmpTextRespuestaCorrectaEvaluacion.SetNativeSize();
                    tmpTextRespuestaCorrectaEvaluacion.transform.localScale = Vector3.one * 0.6f;
                }
                else
                {
                    var tmpTextRespuestaCorrectaEvaluacion = Instantiate(prefabTextPreguntaRespuesta, transformListaRespuestasUsuario).GetComponent<Text>();
                    tmpTextRespuestaCorrectaEvaluacion.text = argRespuestasUsuario[i];
                }

                var tmpTextEnumeracion = Instantiate(prefabTextEnumeracion, transformEnumeracionRespuestasUsuario).GetComponent<Text>();
                tmpTextEnumeracion.text = (i + 1) + ". ";
            }
        }

        public void SetPreguntasCuaderno(string[] argPreguntasCuaderno)
        {
            while (transformPadrePreguntasCuaderno.childCount > 0)
                DestroyImmediate(transformPadrePreguntasCuaderno.GetChild(0).gameObject);


            preguntasCuaderno = Instantiate(prefabPreguntaCuaderno, transformPadrePreguntasCuaderno).GetComponent<Text>();
            for (int i = 0; i < argPreguntasCuaderno.Length; i++)
            {
        
                preguntasCuaderno.text= preguntasCuaderno.text +argPreguntasCuaderno[i]+"\n";

            }

            /*
                        preguntasCuaderno = new Text[argPreguntasCuaderno.Length];

                        for (int i = 0; i < argPreguntasCuaderno.Length; i++)
                        {
                            preguntasCuaderno[i] = Instantiate(prefabPreguntaCuaderno, transformPadrePreguntasCuaderno).GetComponent<Text>();
                            preguntasCuaderno[i].text = argPreguntasCuaderno[i];
                            Debug.Log("Crear pregunta " + argPreguntasCuaderno[i]);
                        }
                        */
        }

        #endregion
    }
}