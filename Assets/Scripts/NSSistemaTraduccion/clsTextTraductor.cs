﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSTraduccionIdiomas;
using UnityEngine.UI;
using TMPro;
using System.Text;

public class clsTextTraductor : MonoBehaviour {

    #region  members
    private DiccionarioIdiomas diccionarioIdiomas;

    #endregion


    #region monobehaviour


    private void Awake()
    {
        if (!diccionarioIdiomas)
            diccionarioIdiomas = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        diccionarioIdiomas.DLTraducirForNameObj += traducirGameObjetText;
    }

    // Use this for initialization
    void Start () {
        traducirGameObjetText();

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    #endregion

    #region public  methods
    #endregion

    #region private methods
    /// <summary>
    /// identifica el tipo de texto y lo traduse
    /// </summary>
    private void traducirGameObjetText()
    {

        if (this.GetComponent<Text>())
        {
            var traduccion = diccionarioIdiomas.Traducir(this.name, this.GetComponent<Text>().text);
            this.GetComponent<Text>().text = traduccion;

        }
        if (this.GetComponent<TextMeshProUGUI>())
        {
            

            var traduccion = diccionarioIdiomas.Traducir(this.name, this.GetComponent<TextMeshProUGUI>().text);
            var textoConFormato = new StringBuilder(traduccion);
            this.GetComponent<TextMeshProUGUI>().SetText(textoConFormato);
        }
        if (this.GetComponent<TextParaTraducir>())
        {
            var traduccion = diccionarioIdiomas.Traducir(this.name, this.GetComponent<TextParaTraducir>().text);
            this.GetComponent<TextParaTraducir>().text = traduccion;
        }

    }
    #endregion


}
