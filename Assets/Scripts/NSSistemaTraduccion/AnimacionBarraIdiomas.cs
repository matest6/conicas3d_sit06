﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSTraduccionIdiomas
{
    public class AnimacionBarraIdiomas : MonoBehaviour
    {

        #region members

        [SerializeField]
        private Animator refAnimator;
        #endregion

        public void ToggleAnimation(bool argDesplegar)
        {
            refAnimator.SetBool("desplegar", argDesplegar);
        }
    } 
}
