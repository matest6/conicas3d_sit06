﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSBoxMessage;
using NSTraduccionIdiomas;

public class clsHerramientasNotas : MonoBehaviour {

    #region members

    /// <summary>
    /// objeto que contiene los ejes de lso laseres 
    /// </summary>
    public clsLaser ObjetoLaser;

    /// <summary>
    /// prefabricado del punto para la anotacion
    /// </summary>
    public GameObject PuntoParaNota;
    /// <summary>
    /// camara que visualiza el plano y esta activa 
    /// </summary>
    public Camera camaraDelpalno;

    private BoxMessageManager Mensaje;

    /// <summary>
    /// tag del plano cartesioano
    /// </summary>
    public string tagPlanoCartesioano;

    /// <summary>
    /// mensaje para cunado no esta activado el laser y se quiere poner la nota
    /// </summary>
    public string TextoCuandoNoEstaAcctivadoElLaser;

    private DiccionarioIdiomas dicIdiomas;

    #endregion

    #region Delegate

    public delegate void DelegatetMensaje(string mensage);
    
    /// <summary>
    ///  delegado el cual se llamda a la hora de reportar un mesaje importante para el usuario, sele como parametro el mensaje a mostrar
    /// </summary>
    public DelegatetMensaje DltMensaje = delegate (string mensage) { };

    #endregion

    #region accesors

    #endregion

    #region events

    #endregion

    #region monoBehaviour

    void Awake()
    {
        dicIdiomas = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
    }

    void Start()
    {
       Mensaje= GameObject.FindGameObjectWithTag("Mensaje").GetComponent<BoxMessageManager>();
    }

    private void Update()
    {
       
    }
    #endregion

    #region private methods

    #endregion


    #region public methods2

    public void mtdBorrarPuntosYNotas() {
        var ListaNotas = gameObject.GetComponentsInChildren<clsPuntoParaNota>();
        if (ListaNotas!=null) {
            for (int i = 0; i < ListaNotas.Length; i++) {
                ListaNotas[i].mtdCerrar();
            }
        }
    }

    /// <summary>
    /// boton que instancia un objeto en un punto
    /// </summary>
    public void mtdAsignarPunto()
    {

        if (ObjetoLaser.isActiveAndEnabled)
        {

            var PosMundo = new Vector3(ObjetoLaser.ObjetoEjey.transform.position.x, 1f, ObjetoLaser.ObjetoEjex.transform.position.z);
            var posFinal = PosMundo;
        
            var puntoNota = Instantiate(PuntoParaNota, posFinal, Quaternion.identity, transform);
            puntoNota.transform.localPosition = PosMundo;
            puntoNota.GetComponent<clsPuntoParaNota>().camaraAcual = camaraDelpalno;

        }
        else
        {
            Mensaje.MtdCreateBoxMessageInfo(dicIdiomas.Traducir(TextoCuandoNoEstaAcctivadoElLaser, "."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
        }



    }



    #endregion




    #region courutines

    #endregion


}
