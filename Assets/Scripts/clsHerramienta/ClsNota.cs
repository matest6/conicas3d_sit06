﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;



namespace NSInterfazAvanzada
{
    public class ClsNota : AbstractPanelInterfaz
    {

        public delegate void DlgMetodoParaBoton();

        public Button Aceptar;
        public Button Eliminar;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

        }

        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
        }


        public void desactivar() {
            ActivarPanel(false);
        }

        /// <summary>
        /// agrega un metodo al boton aceptar
        /// </summary>
        /// <param name="metodo"></param>
        public void AgregarMetodoClick(UnityAction metodo) {
            Aceptar.onClick.AddListener(desactivar);
            Aceptar.onClick.AddListener(metodo);

        }


        /// <summary>
        /// agrega un metodo al boton eliminar 
        /// </summary>
        /// <param name="metodo"></param>
        public void AgregarMetodoEliminar(UnityAction metodo) {
            Eliminar.onClick.AddListener(metodo);
        }




    }


}