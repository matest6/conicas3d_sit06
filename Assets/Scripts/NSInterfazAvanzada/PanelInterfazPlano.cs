﻿using NSBoxMessage;
using NSSistemaSolar;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSTraduccionIdiomas;

namespace NSInterfazAvanzada
{
	public class PanelInterfazPlano : AbstractPanelInterfaz
	{

        #region members
        [SerializeField]
        private SistemaSolarManager refSistemaSolarManager;

        [SerializeField]
        private ControlCamaraPrincipal refControlCamaraPrincipal;

        [SerializeField]
        private ControlCamaraSistemaSolar refControlCamaraSistemaSolar;

        [SerializeField]
        private GameObject joyStick;

        [SerializeField]
        private PanelInterfazBotonesSeleccionSituacion refPanelInterfazBotonesSeleccionSituacion;

        [SerializeField]
        private PanelInterfazSeleccionFuncion refPanelInterfazSeleccionFuncion;

        [SerializeField]
        private clsHerramientasNotas refHerramientasNotas;

        [SerializeField]
        private GameObject laser;


        [SerializeField]
        private BoxMessageManager refBoxMessageManager;
        private DiccionarioIdiomas dic;
        
        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
            dic = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
		}

        #endregion

        #region public methods
        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
        }

        public void OnBotonSalirSituacion()
        {
            refBoxMessageManager.MtdCreateBoxMessageDecision(dic.Traducir("MensjeSalirDeLaSituacion", "¿Desea salir de la situación?"), dic.Traducir("TextbotonSali", "SALIR"), dic.Traducir("TextBotonCancelar", "CANCELAR"), EjecutarSalirSituacion, null);
        }

        public void EjecutarSalirSituacion()
        {
            refPanelInterfazBotonesSeleccionSituacion.ActivarPanel();
            ActivarPanel(false);
            refSistemaSolarManager.SalirSituacion();
            refPanelInterfazSeleccionFuncion.SalirSituacion();
            refControlCamaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoqueMesaHolograma);
            refControlCamaraPrincipal.SetOrthograpicCamera(false);
            refControlCamaraSistemaSolar.SetAccionActual(AccionesCamaraSistemaSolar.enfoquePrimeraPersona);
            refControlCamaraSistemaSolar.SetOrthograpicCamera(false);
            refHerramientasNotas.mtdBorrarPuntosYNotas();
            laser.gameObject.GetComponent<clsLaser>().DesactivarBoton();

        }

        #endregion

        #region courutines

        #endregion
    }
}