﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSTraduccionIdiomas;
namespace NSInterfazAvanzada
{
	public class PanelInterfazSeleccionLenguajeInglesEspaniol : AbstractPanelInterfaz
	{

        #region members

        public DiccionarioIdiomas Dic;

 		#endregion

 		#region accesors

 		#endregion

 		#region events

 		#endregion

 		#region monoBehaviour

 		void Awake ()
 		{
            ActivarPanel();
		}

 		// Use this for initialization
		void Start ()
		{

		}

		// Update is called once per frame
 		void Update ()
		{

		}

        #endregion

        #region private methods

        #endregion

        #region public methods
        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
        }

        /// <summary>
        /// cambia el idioma del simulador a español
        /// </summary>
        public void mtdTraducirEspañol() {
            Dic.SetIdioma(ListaIdiomas.espaniol);
        }

        /// <summary>
        /// ccmabia el idioma del simmulador a ingles
        /// </summary>
        public void mtdTraducirIngles() {
            Dic.SetIdioma(ListaIdiomas.ingles);
        }
            
        #endregion

        #region courutines

        #endregion
    }
}