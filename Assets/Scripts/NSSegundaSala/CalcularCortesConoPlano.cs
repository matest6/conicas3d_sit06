﻿using NSGraficadoFuncionesConicas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSSegundaSala
{
    public class CalcularCortesConoPlano : MonoBehaviour
    {
        [SerializeField]
        private Transform cono;

        [SerializeField]
        private Transform plano;

        [SerializeField]
        private Transform puntoColliderUp;

        [SerializeField]
        private Transform puntoColliderDown;

        private void FixedUpdate()
        {
            CacularPosicionPuntos();
        }

        private void CacularPosicionPuntos()
        {
            var tmpPlaneCollision = new Plane(plano.forward, plano.position);
            var tmpClosestPointOnPlane = tmpPlaneCollision.ClosestPointOnPlane(cono.position);
            puntoColliderUp.position  = tmpClosestPointOnPlane + Vector3.ProjectOnPlane(cono.up * 0.5f * (1 - Vector3.Dot(cono.up, plano.forward)), plano.forward);
            puntoColliderDown.position = tmpClosestPointOnPlane + Vector3.ProjectOnPlane(-cono.up * 0.5f * (1 - Vector3.Dot(-cono.up, plano.forward)), plano.forward);            
        }
    }
}
