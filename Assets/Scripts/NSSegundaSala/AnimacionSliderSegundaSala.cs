﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSSegundaSala
{
    public class AnimacionSliderSegundaSala : MonoBehaviour
    {

        #region members

        [SerializeField]
        private Animator refAnimator;
        #endregion

        // Use this for initialization
        void Start()
        {
            SetAnimacionExpandir(false);
        }

        #region public methods

        public void SetAnimacionExpandir(bool argExpandir)
        {
            refAnimator.SetBool("Expandir", argExpandir);
        }

        #endregion
    }
}
