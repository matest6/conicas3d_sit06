﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class clsInformacion : MonoBehaviour {

    public GameObject[] InformacionSituacion;
    public GameObject[] InformacionProcedimiento;
    public GameObject[] InformacionEcuaciones;

    public Toggle situacion;
    public Toggle procedimento;


    // Use this for initialization
    void Start () {

        MtdActivarSituacion();

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void MtdActivarSituacion()
    {
        DesactivarInfo();
        foreach (GameObject iterador in InformacionSituacion)
        {
            iterador.SetActive(true);
        }
    }

    public void MtdActivarProcedimeinto()
    {
        DesactivarInfo();
        foreach (GameObject iterador in InformacionProcedimiento)
        {
            iterador.SetActive(true);
        }
    }

    public void MtdActivarEcuaciones()
    {
        DesactivarInfo();
        foreach (GameObject iterador in InformacionEcuaciones)
        {
            iterador.SetActive(true);
        }
    }

    public void DesactivarInfo()
    {
        foreach (GameObject iterador in InformacionEcuaciones)
        {
            iterador.SetActive(false);
        }

        foreach (GameObject iterador in InformacionProcedimiento)
        {
            iterador.SetActive(false);
        }

        foreach (GameObject iterador in InformacionSituacion)
        {
            iterador.SetActive(false);
        }

    }

    public void mtdReiniciar()
    {
        situacion.isOn=true;
        procedimento.isOn=false;
        MtdActivarSituacion();
    }



}
